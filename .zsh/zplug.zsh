source $HOME/.zplug/init.zsh

# zplug plugins
# zplug mafredri/zsh-async, from:github
zplug "zsh-users/zsh-autosuggestions", from:github
zplug "zsh-users/zsh-history-substring-search", from:github
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "lukechilds/zsh-nvm", from:github

# zplug theme
# zplug sindresorhus/pure, use:pure.zsh, from:github, as:theme
zplug romkatv/powerlevel10k, use:powerlevel10k.zsh-theme as:theme

# Enable history search
if zplug check zsh-users/zsh-history-substring-search; then
  bindkey '^[[A' history-substring-search-up
  bindkey '^[[B' history-substring-search-down
fi

# Install packages that have not been installed yet
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    else
        echo
    fi
fi

zplug load
