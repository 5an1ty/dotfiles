# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export LC_ALL=en_US.UTF-8  
export LANG=en_US.UTF-8

# Load zplug
source $HOME/.zsh/zplug.zsh

# Source alias / functions:
source $HOME/.zsh/aliases.zsh
source $HOME/.zsh/source_if_possible.zsh

# Source private alias / functions
source_if_possible "$HOME/.private.zsh"

# global ZSH config
HISTFILE=$HOME/.zsh_history
HISTSIZE=10000
SAVEHIST=$HISTSIZE

setopt inc_append_history
setopt share_history

# set extra variables
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=5'

if [ -d $HOME/.deno ]; then
	export PATH=$PATH:$HOME/.deno/bin
fi

if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
